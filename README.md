# Krini

* TODO: Description

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The following software is required. The used versions are indicated:
* [Git, v1.8.3.1](https://git-scm.com/)
* [Docker Community Edition, v17.12.1-ce (build 7390fc6)](https://www.docker.com/community-edition)
* [Docker Compose, v1.21.2 (build a133471)](https://docs.docker.com/compose/)

### Installing

#### Downloading application files

Download wrapper repository including associated submodules

```bash
git clone --recurse-submodules ssh://git@git.scicore.unibas.ch:2222/krini/krini.git
```

Traverse to generated directory

```bash
cd krini
```

Switch to submodule branches configured in `.gitmodules` (otherwise submodules point to detached head)

```bash
git submodule foreach 'branch="$(git config -f ../.gitmodules submodule.$name.branch)"; git checkout $branch'
```

#### Updating application files

When updating the superproject including all submodules, do:

```bash
git pull origin dev
git submodule sync
git submodule foreach git pull origin dev
```

* Try this out instead
* `git submodule sync --recursive`
* `git submodule update --recursive --remote`

#### Configuring application

* TODO

#### Building application

```bash
docker-compose build
```

#### Deploying application

```bash
docker-compose up -d
```

#### Shutting down application
```bash
docker-compose down --volumes
```

## Running tests

TODO: Instructions / subsections for unit, integration, e2e etc tests

## Built With

* [Django](https://www.djangoproject.com/)
* [Django REST framework](http://www.django-rest-framework.org/)
* [Angular](https://angular.io/)
* [Docker](https://www.docker.com/)
* [CWL](https://github.com/common-workflow-language/common-workflow-language)

## Authors

### Main authors

* [**Alexander Kanitz**](https://git.scicore.unibas.ch/kanitz)
* [**Foivos Gypas**](https://git.scicore.unibas.ch/gypas)
 
### Contributors

In alphabetical order:

* [**Pablo Escobar**](https://git.scicore.unibas.ch/escobar)
* [**Christina Herrmann**](https://git.scicore.unibas.ch/herrmchr)
* [**Patrick Keller**](https://git.scicore.unibas.ch/mixupy97)
* [**Marcel Luckeneder**](https://git.scicore.unibas.ch/cogipa10)
* [**Edim Zrdalovic**](https://git.scicore.unibas.ch/cuhoni66)

* TODO: Contributors file

## License

This project is licensed under the Apache 2.0 License. See the [LICENSE](LICENSE) file for details.

## Acknowledgments

* TODO