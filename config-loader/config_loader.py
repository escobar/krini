import sys
import os
import yaml
import json

class ConfigLoader:

    def __init__(self, master_config):
        file_path = master_config
        yaml_file = open(file_path,'r').read()
        self.config = yaml.load(yaml_file)

    def createGlobalConfig(self, out_path):
        storage_config = self.config['global']['storage']
        global_config = storage_config.copy()
        with open(os.path.join(out_path, 'globalconfig.json'), 'w') as outfile:
            json.dump(global_config, outfile)

    def createDjangoConfig(self, out_path):
        network_config = self.config['krini-dr']['network']
        database_config = self.config['krini-dr']['database']
        env_config = self.config['krini-dr']['environment']
        django_config = network_config.copy()
        django_config.update(database_config)
        django_config.update(env_config)
        with open(os.path.join(out_path, 'djangoconfig.json'), 'w') as outfile:
            json.dump(django_config, outfile)

    def createAngularConfig(self, out_path):
        network_config = self.config['krini-ng']['network']
        env_config = self.config['krini-ng']['environment']
        angular_config = network_config.copy()
        angular_config.update(env_config)
        with open(os.path.join(out_path, 'angularconfig.json'), 'w') as outfile:
            json.dump(angular_config, outfile)

    def createFlaskConfig(self, out_path):
        env_config = self.config['krini-cwl']['environment']
        flask_config = env_config.copy()
        with open(os.path.join(out_path, 'flaskconfig.json'), 'w') as outfile:
            json.dump(flask_config, outfile)

if __name__ == '__main__':
    master_config = sys.argv[1]
    out_path = sys.argv[2]
    loader = ConfigLoader(master_config)
    loader.createGlobalConfig(out_path)
    loader.createAngularConfig(out_path)
    loader.createDjangoConfig(out_path)
    loader.createFlaskConfig(out_path)
